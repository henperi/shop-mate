import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { CartComponent } from '@modules/cart/cart.component';
import { LandingComponent } from '@modules/landing/landing.component';
import { MenuComponent } from '@modules/menu/pages/menu/menu.component';
import { NotFoundComponent } from '@modules/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    pathMatch: 'full',
  },
  {
    path: 'cart',
    component: CartComponent,
  },
  {
    path: 'menu',
    component: MenuComponent,
    loadChildren: (): Promise<any> =>
      import('./modules/menu/menu.module').then((m: any) => m.MenuModule),
  },
  {
    path: 'orders',
    loadChildren: (): Promise<any> =>
      import('./modules/orders/orders.module').then((m: any) => m.OrdersModule),
  },
  // Fallback when no prior routes is matched
  { path: '**', component: NotFoundComponent, pathMatch: 'full' },
];

/**
 * AppRoutingModule class
 */
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
