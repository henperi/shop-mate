import { Component, OnInit } from '@angular/core';

// Interfaces
import { Menu } from '@interfaces/menu-item.interface';

// Services
import { MenuService } from '@services/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  menu: Menu[];
  pizzaMenu: Menu[];
  drinksMenu: Menu[];

  constructor(private menuService: MenuService) {}

  ngOnInit() {
    Promise.all([this.getMenu(), this.getPizza(), this.getDrinks()]);
  }

  private getMenu(): void {
    this.menuService
      .getAllMenuItems()
      .subscribe((menu: Menu[]) => (this.menu = menu))
      .unsubscribe();
  }

  private getPizza(): void {
    this.menuService
      .getPizza()
      .subscribe((pizzaMenu: Menu[]) => (this.pizzaMenu = pizzaMenu))
      .unsubscribe();
  }

  private getDrinks(): void {
    this.menuService
      .getDrinks()
      .subscribe((drinksMenu: Menu[]) => (this.drinksMenu = drinksMenu))
      .unsubscribe();
  }
}
