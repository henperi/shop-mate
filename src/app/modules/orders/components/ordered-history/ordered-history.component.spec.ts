import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedHistoryComponent } from './ordered-history.component';

describe('OrderedHistoryComponent', () => {
  let component: OrderedHistoryComponent;
  let fixture: ComponentFixture<OrderedHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderedHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
