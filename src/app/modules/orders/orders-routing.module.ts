import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { OrderedHistoryComponent } from './components/ordered-history/ordered-history.component';
import { OrderedItemsComponent } from './components/ordered-items/ordered-items.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'ordered-items',
    pathMatch: 'full',
  },
  {
    path: 'ordered-items',
    component: OrderedItemsComponent,
  },
  {
    path: 'order-history',
    component: OrderedHistoryComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersRoutingModule {}
