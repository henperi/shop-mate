import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Modules
import { OrdersRoutingModule } from './orders-routing.module';

// Components
import { OrderedHistoryComponent } from './components/ordered-history/ordered-history.component';
import { OrderedItemsComponent } from './components/ordered-items/ordered-items.component';

@NgModule({
  declarations: [OrderedItemsComponent, OrderedHistoryComponent],
  imports: [CommonModule, OrdersRoutingModule],
})
export class OrdersModule {}
