import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';

// Components
import { FlexLayoutModule } from '@angular/flex-layout';
import { ButtonComponent } from '@sharedComponents/button/button.component';
import { HeroComponent } from '@sharedComponents/hero/hero.component';
import { ItemCardComponent } from './components/item-card/item-card.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { materialDesignImports } from './materialDesign';

@NgModule({
  declarations: [
    ButtonComponent,
    HeroComponent,
    ToolbarComponent,
    ItemCardComponent,
  ],
  imports: [CommonModule, ...materialDesignImports, FlexLayoutModule],
  exports: [
    ButtonComponent,
    HeroComponent,
    ToolbarComponent,
    ItemCardComponent,
    FlexLayoutModule,
    ...materialDesignImports,
  ],
})
export class SharedModule {}
