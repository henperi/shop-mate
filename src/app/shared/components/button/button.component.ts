import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() label: string;
  @Input() color?: string;
  @Input() chubby?: boolean;
  @Input() classNames?: string;
  @Output() clickAction?: EventEmitter<any> = new EventEmitter<any>();

  handleClick(event: any): void {
    return this.clickAction.emit(event);
  }
}
