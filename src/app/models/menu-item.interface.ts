export interface Menu {
  id: string;
  name: string;
  image: string;
  price: number;
}
