import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// const httpOptions = {};

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  backendBaseUrl: string = 'www.demo-backend.com/api/v1';
  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getRequest(endpoint: string, options: any = {}): Observable<any> {
    return this.http.get(this.backendBaseUrl.concat(endpoint), options);
  }

  postRequest(
    endpoint: string,
    data: any,
    httpOptions: any = this.httpOptions,
  ): Observable<any> {
    return this.http.post(
      this.backendBaseUrl.concat(endpoint),
      data,
      httpOptions,
    );
  }

  updateRequest(
    endpoint: string,
    data: any,
    httpOptions: any = this.httpOptions,
  ): Observable<any> {
    return this.http.put(
      this.backendBaseUrl.concat(endpoint),
      data,
      httpOptions,
    );
  }

  deleteRequest(
    endpoint: string,
    httpOptions: any = this.httpOptions,
  ): Observable<any> {
    return this.http.delete(this.backendBaseUrl.concat(endpoint), httpOptions);
  }
}
