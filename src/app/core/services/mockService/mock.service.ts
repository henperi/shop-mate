import { Injectable } from '@angular/core';
import { Menu } from '@interfaces/menu-item.interface';
import { drinksArray, menuArray, pizzaArray } from '@shared/mocks/menu';
import { of, throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MockService {
  /**
   * Mock Request for development purpose only
   * @param endpoint the endpoint to call with preceeding '/'
   * @param params  the params object
   */
  getRequest<T>(endpoint: string, params?: any): Observable<any> {
    switch (endpoint) {
      case '/menu':
        if (!params) {
          return of(menuArray);
        }
        if (params.id) {
          const menuItem = menuArray.find(
            (item: Menu) => item.id === params.id,
          );

          return menuItem
            ? of(menuItem)
            : throwError('No menu item has this id');
        }

        return throwError('Unable to resolve request');

      case '/menu/pizza':
        if (!params) {
          return of(pizzaArray);
        }
        if (params.id) {
          const pizzaItem = pizzaArray.find(
            (item: Menu) => item.id === params.id,
          );

          return pizzaItem
            ? of(pizzaItem)
            : throwError('No pizza with this id');
        }

        return of('Not found');

      case '/menu/drinks':
        if (!params) {
          return of(drinksArray);
        }
        if (params.id) {
          const drinksItem = drinksArray.find(
            (item: Menu) => item.id === params.id,
          );

          return drinksItem
            ? of(drinksItem)
            : throwError('No drinks with this id');
        }

        return throwError('Unable to resolve request');

      default:
        return throwError('Unable to resolve request');
    }
  }
}
