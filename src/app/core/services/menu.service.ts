import { Injectable } from '@angular/core';

// RxJs
import { of, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

// Services
import { HttpErrorResponse } from '@angular/common/http';
import { Menu } from '@interfaces/menu-item.interface';
import { MockService } from './mockService/mock.service';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  constructor(private mockService: MockService) {}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation: string, result?: T): any {
    return (error: HttpErrorResponse): Observable<T> => {
      if (error.error instanceof ErrorEvent) {
        console.error(`Client error ${error}`, operation);
      } else {
        console.error(`Backend error ${error}`, operation);
      }

      // TODO: Transform error for user consumption (Toaster)
      return of(result as T);
    };
  }

  getAllMenuItems(): Observable<Menu[]> {
    return this.mockService.getRequest<Menu[]>('/menu').pipe(
      retry(3),
      catchError(this.handleError<Menu[]>('getAllMenuItems', [])),
    );
  }

  getPizza(): Observable<Menu[]> {
    return this.mockService.getRequest<Menu[]>('/menu/pizza').pipe(
      retry(3),
      catchError(this.handleError<Menu[]>('getPizza', [])),
    );
  }

  getDrinks(): Observable<Menu[]> {
    return this.mockService.getRequest<Menu[]>('/menu/drinks').pipe(
      retry(3),
      catchError(this.handleError<Menu[]>('getDrinks', [])),
    );
  }
}
